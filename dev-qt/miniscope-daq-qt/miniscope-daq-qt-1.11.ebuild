# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qmake-utils

DESCRIPTION="Software for streaming and controlling neural and behavioral data."
HOMEPAGE="https://github.com/Aharoni-Lab/Miniscope-DAQ-QT-Software"
SRC_URI="https://github.com/HaaPut/Miniscope-DAQ-QT-Software/archive/refs/heads/master.zip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	dev-qt/qt3d:5
	dev-lang/python
	dev-python/numpy
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/Miniscope-DAQ-Software_build_v1_11"

# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..10} )
inherit distutils-r1

DESCRIPTION="Markerless pose-estimation of user-defined features with deep learning"
HOMEPAGE="https://www.mousemotorlab.org/deeplabcut/"

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="https://github.com/DeepLabCut/DeepLabCut/"
	inherit git-r3
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="LGPL-3"
SLOT="0"
IUSE="cuda wxwidgets"

RDEPEND="
	dev-python/click[${PYTHON_USEDEP}]
	dev-python/imgaug[${PYTHON_USEDEP}]
	dev-python/ipython[${PYTHON_USEDEP}]
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/networkx[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/pandas[${PYTHON_USEDEP}]
	dev-python/pyyaml[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]
	dev-python/statsmodels[${PYTHON_USEDEP}]
	dev-python/ruamel-yaml[${PYTHON_USEDEP}]
	media-libs/opencv[${PYTHON_USEDEP}]
	media-video/ffmpeg
	sci-libs/scikit-image[${PYTHON_USEDEP}]
	sci-libs/scikit-learn[${PYTHON_USEDEP}]
	sci-libs/tensorflow[${PYTHON_USEDEP}]

	wxwidgets? (
		dev-python/wxpython[${PYTHON_USEDEP}]
	)
	cuda? (
		media-libs/opencv[${PYTHON_USEDEP},cuda]
		sci-libs/tensorflow[${PYTHON_USEDEP},cuda]
	)
"

# S="${WORKDIR}/CaImAn-${PV}"

# python_install() {
	# distutils-r1_python_install
	# "${EPYTHON}" caimanmanager.py install
# }

# Copyright 2020-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..10} )
inherit distutils-r1

DESCRIPTION="A Python toolbox for large scale Calcium Imaging data and behavioral analysis."
HOMEPAGE="https://github.com/flatironinstitute/CaImAn"
SRC_URI="https://github.com/flatironinstitute/CaImAn/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="cuda"

BDEPEND="
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/cython[${PYTHON_USEDEP}]
"
RDEPEND="
	${BDEPEND}
	>=dev-python/h5py-2.10.0[${PYTHON_USEDEP}]
	>=sci-libs/scikit-image-0.19.0[${PYTHON_USEDEP}]
	dev-python/future[${PYTHON_USEDEP}]
	dev-python/holoviews[${PYTHON_USEDEP}]
	dev-python/ipyparallel[${PYTHON_USEDEP}]
	dev-python/ipython[${PYTHON_USEDEP}]
	dev-python/jupyter[${PYTHON_USEDEP}]
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/pandas[${PYTHON_USEDEP}]
	dev-python/peakutils[${PYTHON_USEDEP}]
	dev-python/pims[${PYTHON_USEDEP}]
	dev-python/pillow[${PYTHON_USEDEP}]
	dev-python/psutil[${PYTHON_USEDEP}]
	dev-python/pynwb[${PYTHON_USEDEP}]
	dev-python/pyparsing[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]
	dev-python/tifffile[${PYTHON_USEDEP}]
	dev-python/tqdm[${PYTHON_USEDEP}]
	media-libs/opencv[${PYTHON_USEDEP}]
	sci-libs/scikit-learn[${PYTHON_USEDEP}]
	sci-libs/tensorflow[${PYTHON_USEDEP}]

	cuda? (
		dev-python/pycuda[${PYTHON_USEDEP}]
		media-libs/opencv[${PYTHON_USEDEP},cuda]
		sci-libs/tensorflow[${PYTHON_USEDEP},cuda]
	)
"

S="${WORKDIR}/CaImAn-${PV}"

python_install() {
	distutils-r1_python_install
	"${EPYTHON}" caimanmanager.py install
}

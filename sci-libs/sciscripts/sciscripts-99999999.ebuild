# Copyright 2020-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..10} )

inherit distutils-r1

DESCRIPTION="Scripts for controlling devices/running experiments/analyzing data"
HOMEPAGE="https://gitlab.com/malfatti/SciScripts"

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="$HOMEPAGE"
	EGIT_BRANCH="main"
	inherit git-r3
elif [[ ${PV} == "99999999" ]] ; then
	EGIT_REPO_URI="$HOMEPAGE"
	EGIT_BRANCH="Dev"
	inherit git-r3
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

IUSE="+asdf +hdf5 +serial +soundcard +statistics +video"

LICENSE="GPL-3"
SLOT="0"

RDEPEND="
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]

	asdf? ( dev-python/asdf[${PYTHON_USEDEP}] )
	hdf5? ( dev-python/h5py[${PYTHON_USEDEP}] )
	serial? ( dev-python/pyserial[${PYTHON_USEDEP}] )
	soundcard? (
		dev-python/pandas[${PYTHON_USEDEP}]
		dev-python/pytables[${PYTHON_USEDEP}]
		dev-python/sounddevice[${PYTHON_USEDEP}]
	)
	video? (
		dev-python/pandas[${PYTHON_USEDEP}]
		dev-python/pytables[${PYTHON_USEDEP}]
		media-libs/opencv[${PYTHON_USEDEP}]
	)
	statistics? (
		dev-python/pandas[${PYTHON_USEDEP}]
		dev-python/pytables[${PYTHON_USEDEP}]
		dev-python/rpy[${PYTHON_USEDEP}]
		dev-python/statsmodels[${PYTHON_USEDEP}]
		dev-python/scikit-posthocs[${PYTHON_USEDEP}]
		dev-python/unidip[${PYTHON_USEDEP}]
	)
"

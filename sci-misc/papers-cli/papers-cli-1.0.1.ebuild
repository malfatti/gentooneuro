# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..10} )

inherit distutils-r1

DESCRIPTION="Command-line tool to manage bibliography (pdfs + bibtex)"
HOMEPAGE="https://github.com/perrette/papers"

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="$HOMEPAGE"
	EGIT_BRANCH="master"
	inherit git-r3
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${PN}-v${PV}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"
IUSE="doc test"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-python/bibtexparser[${PYTHON_USEDEP}]
	dev-python/crossrefapi[${PYTHON_USEDEP}]
	dev-python/fuzzywuzzy[${PYTHON_USEDEP}]
	dev-python/scholarly[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
	dev-python/unidecode[${PYTHON_USEDEP}]
	!sci-misc/papers
"

DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	doc? ( dev-python/sphinx[${PYTHON_USEDEP}] )
	test? ( dev-python/pytest[${PYTHON_USEDEP}] )
"

# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Taken and modified from bgo-overlay @ https://gpo.zugaina.org/dev-libs/liblzf

EAPI="7"

DESCRIPTION="LibLZF is a very small, very fast data compression library"
HOMEPAGE="http://oldhome.schmorp.de/marc/liblzf.html"
SRC_URI="http://dist.schmorp.de/liblzf/lib${P}.tar.gz"

S="${WORKDIR}/lib${P}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

src_prepare() {
	sed -i -e 's/^   CFLAGS=.*$/   :/' configure.ac || die "sed failed"
	sed -i -e 's/\(\$([a-z]*dir)\)/$(DESTDIR)\1/' Makefile.in || die "sed failed"
	eapply_user
}

src_install() {
	emake install DESTDIR="${ED}"
	dodoc Changes README
}

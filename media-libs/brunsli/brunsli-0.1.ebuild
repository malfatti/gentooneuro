# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit cmake git-r3

DESCRIPTION="Brunsli is a lossless JPEG repacking library."
HOMEPAGE="https://github.com/google/brunsli"

EGIT_REPO_URI="$HOMEPAGE"
EGIT_COMMIT="v${PV}"
EGIT_SUBMODULES=(
	third_party/brotli
	third_party/googletest
	third_party/highwayhash
)

LICENSE="MIT"
SLOT="0"
# KEYWORDS="~amd64"

# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{10..13} )

inherit python-r1

DESCRIPTION="Scripts for controlling devices/running experiments/analyzing data"
HOMEPAGE="https://gitlab.com/malfatti/SciScripts"
KEYWORDS="~amd64 ~x86"

LICENSE="GPL-3"
SLOT="0"

	# $(python_gen_cond_dep 'dev-python/scikit-posthocs[${PYTHON_USEDEP}]' 'python*')
RDEPEND="
	$(python_gen_cond_dep 'dev-python/asdf[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/h5py[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/matplotlib[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/numpy[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/pandas[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/pyserial[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/tables[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/rpy2[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/scipy[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/sounddevice[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/statsmodels[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/unidip[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'media-libs/opencv[${PYTHON_USEDEP}]' 'python*')
"

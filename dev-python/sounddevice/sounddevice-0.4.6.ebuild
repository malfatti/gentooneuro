# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..12} )

inherit distutils-r1

DESCRIPTION="Play and Record Sound with Python"
HOMEPAGE="https://github.com/spatialaudio/python-sounddevice"

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="$HOMEPAGE"
	EGIT_BRANCH="master"
	inherit git-r3
else
	inherit pypi
	# SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc test"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-python/numpy[${PYTHON_USEDEP}]
	>=dev-python/cffi-1.0[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	doc? ( dev-python/sphinx[${PYTHON_USEDEP}] )
	test? ( dev-python/pytest[${PYTHON_USEDEP}] )
"

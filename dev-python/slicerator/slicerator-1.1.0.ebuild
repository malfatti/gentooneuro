# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )
inherit distutils-r1 pypi

DESCRIPTION="A lazy-loading, fancy-sliceable iterable."
HOMEPAGE="https://github.com/soft-matter/slicerator"
# SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

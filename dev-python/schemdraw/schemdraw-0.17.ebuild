# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v3

EAPI=8
PYTHON_COMPAT=( python3_{8..12} )

inherit distutils-r1 pypi
DISTUTILS_USE_PEP517=setuptools

DESCRIPTION="Electrical circuit schematic drawing"
HOMEPAGE="https://github.com/cdelker/schemdraw"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )
inherit distutils-r1 pypi

DESCRIPTION="This package provides utilities related to the detection of peaks on 1D data."
HOMEPAGE="https://peakutils.readthedocs.io/en/latest"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/PeakUtils-${PV}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]
"

S="${WORKDIR}/PeakUtils-${PV}"

# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..10} )
inherit distutils-r1

DESCRIPTION="Statistical post-hoc analysis and outlier detection algorithms"
HOMEPAGE="https://github.com/maximtrp/scikit-posthocs"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

	# dev-python/seaborn[${PYTHON_USEDEP}]
RDEPEND="
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/pandas[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]
	dev-python/statsmodels[${PYTHON_USEDEP}]
"

_distutils-r1_post_python_install() {
	debug-print-function ${FUNCNAME} "${@}"

	local sitedir=${D%/}$(python_get_sitedir)
	if [[ -d ${sitedir} ]]; then
		_distutils-r1_strip_namespace_packages "${sitedir}"

		local forbidden_package_names=(
			examples test tests
			.pytest_cache .hypothesis _trial_temp
		)
		local strays=()
		local p
		mapfile -d $'\0' -t strays < <(
			find "${sitedir}" -maxdepth 1 -type f '!' '(' \
					-name '*.egg-info' -o \
					-name '*.pth' -o \
					-name '*.py' -o \
					-name '*.pyi' -o \
					-name "*$(get_modname)" \
				')' -print0
		)
		for p in "${forbidden_package_names[@]}"; do
			[[ -d ${sitedir}/${p} ]] && strays+=( "${sitedir}/${p}" )
		done
	fi
}

# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )
inherit distutils-r1 pypi

DESCRIPTION="Image transformation, compression, and decompression codecs"
HOMEPAGE="https://www.cgohlke.com"
# SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
# KEYWORDS="~amd64"

RDEPEND="
	app-arch/brotli
	app-arch/bzip2
	app-arch/libdeflate
	app-arch/lz4
	app-arch/lzma
	app-arch/snappy
	app-arch/zopfli
	app-arch/zstd
	dev-libs/bitshuffle
	dev-libs/lzf
	dev-libs/zfp
	dev-python/blosc[${PYTHON_USEDEP}]
	dev-python/cython[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	media-libs/libaom
	media-libs/brunsli
	media-libs/charls
	media-libs/dav1d
	media-libs/giflib
	media-libs/jxrlib
	media-libs/lcms
	media-libs/lerc
	media-libs/libavif
	media-libs/libjpeg-turbo
	media-libs/libpng
	media-libs/libwebp
	media-libs/openjpeg
	media-libs/tiff
	media-video/rav1e
	sci-libs/libaec
	sys-libs/zlib
"

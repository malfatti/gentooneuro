# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )
inherit distutils-r1 pypi

DESCRIPTION="The easy way to write your own Pandas flavor."
HOMEPAGE="https://github.com/Zsailer/pandas_flavor"
# SRC_URI="mirror://pypi/${PN:0:1}/${PN//-/_}/${PN//-/_}-${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-python/lazy-loader[${PYTHON_USEDEP}]
"

S="${WORKDIR}/${PN//-/_}-${PV}"

# Copyright 2020-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )

inherit distutils-r1

DESCRIPTION="Library with functions to iterate through the Crossref API."
HOMEPAGE="https://github.com/fabiobatalha/crossrefapi"

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="$HOMEPAGE"
	EGIT_BRANCH="master"
	inherit git-r3
else
	inherit pypi
	# SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
fi

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc test"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-python/requests[${PYTHON_USEDEP}]
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	doc? ( dev-python/sphinx[${PYTHON_USEDEP}] )
	test? ( dev-python/pytest[${PYTHON_USEDEP}] )
"

PATCHES=( "${FILESDIR}"/${P}.patch )

# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )
inherit distutils-r1 pypi

DESCRIPTION="EditorConfig makes it easy to maintain the correct coding style."
HOMEPAGE="https://editorconfig.org/"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/EditorConfig-${PV}.tar.gz"

S="${WORKDIR}/EditorConfig-${PV}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-libs/libpcre2
"

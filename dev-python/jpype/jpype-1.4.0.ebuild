# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..10} )
inherit distutils-r1 pypi

DESCRIPTION="JPype is a Python module to provide full access to Java from within Python."
HOMEPAGE="https://github.com/jpype-project/jpype"
SRC_URI="mirror://pypi/${PN:0:1}/jpype1/JPype1-${PV}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc test"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-python/typing-extensions[${PYTHON_USEDEP}]
"

BDEPEND="
	doc? (
		dev-python/pygments[${PYTHON_USEDEP}]
		dev-python/setuptools[${PYTHON_USEDEP}]
		dev-python/docutils[${PYTHON_USEDEP}]
		dev-python/mock[${PYTHON_USEDEP}]
		dev-python/commonmark[${PYTHON_USEDEP}]
		dev-python/recommonmark[${PYTHON_USEDEP}]
		dev-python/sphinx[${PYTHON_USEDEP}]
		dev-python/sphinx-rtd-theme[${PYTHON_USEDEP}]
		dev-python/readthedocs-sphinx-ext[${PYTHON_USEDEP}]
	)
	test? (
		dev-python/jedi[${PYTHON_USEDEP}]
		dev-python/pytest[${PYTHON_USEDEP}]
	)
"

S="${WORKDIR}/JPype1-${PV}"

# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )
inherit distutils-r1

DESCRIPTION="A Python module for reading data from a plot provided as SVG file."
HOMEPAGE="https://github.com/peterstangl/svg2data"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

if [[ ${PV} == "9999" ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/peterstangl/svg2data"
else
	inherit pypi
	SRC_URI="https://github.com/peterstangl/${PN}/archive/refs/tags/v${PV}.tar.gz"
fi

RDEPEND="
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/matplotlib[${PYTHON_USEDEP}]
"

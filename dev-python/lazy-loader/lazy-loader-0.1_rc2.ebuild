# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..11} )

DISTUTILS_USE_PEP517=flit
inherit distutils-r1 pypi

DESCRIPTION="lazy-loader makes it easy to load subpackages and functions on demand."
HOMEPAGE="https://scientific-python.org/specs/spec-0001/"
# SRC_URI="mirror://pypi/${PN:0:1}/${PN//-/_}/${PN//-/_}-${PV/_/}.tar.gz"

S="${WORKDIR}/${PN//-/_}-${PV/_/}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

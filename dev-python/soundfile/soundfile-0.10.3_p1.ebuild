# Copyright 2020-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..11} )

inherit distutils-r1

DESCRIPTION="SoundFile is an audio library based on libsndfile, CFFI and NumPy"
HOMEPAGE="https://github.com/bastibe/python-soundfile"
MY_P="${P/soundfile/SoundFile}"
MY_P=${MY_P/_p/.post}

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="$HOMEPAGE"
	EGIT_BRANCH="master"
	inherit git-r3
else
	inherit pypi
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${MY_P}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="BSD"
SLOT="0"
IUSE="doc test"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-python/cffi[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	doc? ( dev-python/sphinx[${PYTHON_USEDEP}] )
	test? ( dev-python/pytest[${PYTHON_USEDEP}] )
"

S="${WORKDIR}/${MY_P}"

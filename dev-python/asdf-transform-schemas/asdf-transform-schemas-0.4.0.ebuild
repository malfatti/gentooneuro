# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8..12} )
inherit distutils-r1 pypi

DESCRIPTION="ASDF schemas for transforms."
HOMEPAGE="https://asdf-transform-schemas.readthedocs.io/en/stable"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

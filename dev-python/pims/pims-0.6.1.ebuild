# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{7..10} )
inherit distutils-r1 pypi

DESCRIPTION="Python Image Sequence library."
HOMEPAGE="https://github.com/soft-matter/pims"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P^^}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RDEPEND="
	dev-python/av[${PYTHON_USEDEP}]
	dev-python/imageio[${PYTHON_USEDEP}]
	dev-python/jinja[${PYTHON_USEDEP}]
	dev-python/jpype[${PYTHON_USEDEP}]
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
	dev-python/slicerator[${PYTHON_USEDEP}]
	dev-python/tifffile[${PYTHON_USEDEP}]
	sci-libs/scikit-image
	doc? (
		dev-python/sphinx_rtd_theme[${PYTHON_USEDEP}]
	)
"

S="${WORKDIR}/${P^^}"

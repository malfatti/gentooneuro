# Copyright 2022-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v3

EAPI=8
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..11} )
inherit distutils-r1 pypi

DESCRIPTION="OCRmyPDF adds an OCR text layer to scanned PDF files"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="
	app-text/tesseract
	app-text/pdfminer[${PYTHON_USEDEP}]
	dev-python/coloredlogs[${PYTHON_USEDEP}]
	dev-python/reportlab[${PYTHON_USEDEP}]
	dev-python/pillow[${PYTHON_USEDEP},lcms]
	media-gfx/img2pdf[${PYTHON_USEDEP}]
"

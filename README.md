# GentooNeuro

Gentoo overlay with ebuilds written for a neuroscience lab. This overlay relies on [science](https://gitweb.gentoo.org/proj/sci.git) and [guru](https://wiki.gentoo.org/wiki/Project:GURU) overlays.


## Installation
```sh
# emerge --ask app-eselect/eselect-repository
# eselect repository enable science guru
# eselect repository add GentooNeuro git https://gitlab.com/malfatti/GentooNeuro
```


## Packages

The main packages are listed below, and the remaining packages are their dependencies.

### app-text/ocrmypdf
OCRmyPDF adds an OCR text layer to scanned PDF files. Very useful for making scanned PDFs searchable.

### dev-python/sounddevice
Play and record sound with python.

### sci-biology/open-ephys-gui
Software for processing, recording, and visualizing multichannel electrophysiological data.  
This is a highly experimental ebuild, where I'm trying to compile the plugins instead of using binary releases. DO NOT USE FOR REAL EXPERIMENTS. Use the ebuilds at the gentoo science overlay instead.

### sci-libs/caiman
A Python toolbox for large scale Calcium Imaging data and behavioral analysis.

### sci-libs/deeplabcut
Markerless pose-estimation of user-defined features with deep learning.

### sci-libs/sciscripts
Scripts for controlling devices/running experiments/analyzing data.

### sci-misc/papers
Simple reference manager in Python.

### sci-misc/papers-cli
Command-line tool to manage bibliography (pdfs + bibtex).


## `pkgcheck` output

Upon running `pkgcheck scan --net`, the following output will not likely be fixed:

```
dev-python/asdf: NonsolvableDeps
```
The `dev-python/gwcs` is only needed for the `test` flag. This package will be dropped as soon as upstream overlays enable python3.10 support to it.


```
dev-python/pyfim: HttpsUrlAvailable
```
Not really. If `http` is replaced in the homepage by `https` a `SSLCertificateError` is returned.


```
dev-python/svg2data: BadFilename
```
That's the upstream name :/


```
sci-libs/sciscripts: DuplicateEclassInherit
```
This is needed for the `if` statement used to inherit git only in live ebuilds.



